describe('Access to signin form ', function() {
    it('finds the content "signin"', function () {
        cy.visit('https://websandbox.io')

        cy.visit('https://websandbox.io/sign-in')

        cy.get('#formulate--sign-in-1').type('websandbox@gmail.com')

        cy.get('#formulate--sign-in-2').type('test')

        cy.get('.formulate-input-element--submit--label')
    })
})