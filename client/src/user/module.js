import { getCurrentUser } from './service'

const user = {
    id: '',
    token: localStorage.getItem('user-token') || '',
    email: '',
    username: '',
    account_type: ''
}

const getters = {
    user: state => state.user
}

const mutations = {
    EDIT_USER: (state, user) => {
        state.user.id = user.id; 
        state.user.email = user.email;
        state.user.token = user.token;
        state.user.username = user.username;
        state.user.account_type = user.account_type;
    },
}

const actions = {
    editUser: (store, user) => {
        store.commit('EDIT_USER', user)
    },
    getUserFromToken: async (store) => {
        const data = await getCurrentUser();
        if (data) {
            store.commit('EDIT_USER', data)
        }
    }
}

export default {
    state: () => ({ user }),
    mutations: mutations,
    actions: actions,
    getters: getters,
}