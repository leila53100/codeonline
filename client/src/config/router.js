import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Product from '../views/Product'
import Pricing from '../views/Pricing'
import SignIn from '../user/SignIn'
import SignUp from '../user/SignUp'
import store from '../config/store'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/product',
        name: 'product',
        component: Product
    },
    {
        path: '/pricing',
        name: 'pricing',
        component: Pricing
    },
    {
        path: '/sign-in',
        name: 'signIn',
        component: SignIn,
        beforeEnter(to, from, next) {
            if (!store.state.user.user.token) {
                next();
            } else {
                next('/');
            }
        }
    },
    {
        path: '/sign-up',
        name: 'signUp',
        component: SignUp,
        beforeEnter(to, from, next) {
            if (!store.state.user.user.token) {
                next();
            } else {
                next('/');
            }
        }
    },
    {
        path: '*',
        redirect: '/'
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router
