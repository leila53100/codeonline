const socket = {}

const mutations = {
    SET_SOCKET: (state, socket) => {
        state.socket = socket;
    }
}

const actions = {
    setSocket: async (store, socket) => {
        store.commit('SET_SOCKET', socket)
    }
}

const getters = {
    socket: state => state.socket
}

export default {
    state: () => ({socket}),
    mutations: mutations,
    actions: actions,
    getters: getters,
}