import {addMemberToProject, createProject, deleteProject, getProjectAndUser} from './service'

const currentProject = {}

const mutations = {
    CURRENT_PROJECT_AND_USERS: (state, project) => {
        state.currentProject = project
    }
}

const actions = {
    createProject: async (data,id) =>{
        await createProject(data,id);
    },
    addMemberToProject: async (data,id) =>{
        await addMemberToProject(data,id);
    },
    deleteProject: async (data) => {
        await deleteProject(data)
    },
    getProjectAndUser: async (store, slug) => {
        const project = await getProjectAndUser(slug)
        store.commit('CURRENT_PROJECT_AND_USERS', project)
    }
}
const getters = {
    usersOnProject: state => state.currentProject.users,
    currentProject: state => state.currentProject,
}

export default {
    state: () => ({ currentProject }),
    mutations: mutations,
    actions: actions,
    getters: getters,
}