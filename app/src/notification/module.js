import { getNotifications } from './service'

const notifications = []

const mutations = {
    GET_NOTIFICATION: (state, notification) => {
        state.notifications = notification
    },
    PUSH_NOTIFICATION: (state, notification) => {
        state.notifications.unshift(notification)
    }

}

const actions = {
    getNotifications: async (store, email) => {
        const notification = await getNotifications(email)
        store.commit('GET_NOTIFICATION', notification)
    },
    pushNotification: async (store, notification) => {
        store.commit('PUSH_NOTIFICATION', notification)
    },
}

const getters = {
    notifications: state => state.notifications,
}

export default {
    state: () => ({notifications}),
    mutations: mutations,
    actions: actions,
    getters: getters,
}