import axios from 'axios'

export const getCodeFromProject = async (id) => {
    return await axios.get(process.env.VUE_APP_API_URL + '/projects/' + id + '/code').then((res) => {
        return res.data
    }).catch(err => {
        if (err.json) {
            return err.json.then(json => {
                console.log(json)
            })
        }
    })
}