import { getCodeFromProject } from './service'

const code = {
    html: '',
    css: '',
    js: ''
}

const mutations = {
    CODE: (state, code) => {
        state.code.html = code.html ? code.html : ''
        state.code.css = code.css ? code.css : ''
        state.code.js = code.js ? code.js : ''
    },
    MODIFY_HTML: (state, value) => {
        state.code.html = value;
    },
    MODIFY_CSS: (state, value) => {
        state.code.css = value;
    },
    MODIFY_JS: (state, value) => {
        state.code.js = value;
    },
}

const actions = {
    modifyHtml: (store, value) => {
        store.commit('MODIFY_HTML', value)
    },
    modifyCss: (store, value) => {
        store.commit('MODIFY_CSS', value)
    },
    modifyJs: (store, value) => {
        store.commit('MODIFY_JS', value)
    },
    getCodeFromProject: async (store, idProject) => {
        const code = await getCodeFromProject(idProject)
        if(code) {
            store.commit('CODE', code)
        }
    }
}

const getters = {
    html: state => state.code.html,
    css: state => state.code.css,
    js: state => state.code.js,
}

export default {
    state: () => ({ code }),
    mutations: mutations,
    actions: actions,
    getters: getters
}