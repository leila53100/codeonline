import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import user from '@/user/module'
import currentProject from '@/project/module'
import code from '@/livecode/module'
import socket from '@/socket/module'
import notifications from '@/notification/module'

export default new Vuex.Store ({
    state: {},
    mutations: {},
    actions: {},
    getters: {},
    modules: {
        user,
        currentProject,
        code,
        socket,
        notifications
    },
})
