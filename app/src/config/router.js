import Vue from 'vue'
import VueRouter from 'vue-router'
import DashboardUser from '../views/DashboardUser'
import DashboardAdminUser from '../views/Admin/DashboardAdminUser'
import DashboardAdminProject from '../views/Admin/DashboardAdminProject'
import DashboardAdminUsers from '../views/Admin/DashboardAdminUsers'
import DashboardAdminProjects from '../views/Admin/DashboardAdminProjects'
import Home from '../views/Home'
import Project from '../views/Project'
import Account from '../views/Account'
import store from '../config/store'


Vue.use(VueRouter)

const routes = [
    {
        path: '',
        name: 'home',
        component: Home
    },
    {
        path: '/projects',
        name: 'dashboardUser',
        component: DashboardUser
    },
    {
        path: '/dashboard-admin/users',
        name: 'dashboardAdminUsers',
        component: DashboardAdminUsers,
        beforeEnter: (to, from, next) => {
            if (store.state.user.user.role_app === 'admin') {
                next()
            } else {
                next({ name: 'dashboardUser' })
            }
        }
    },
    {
        path: '/dashboard-admin/projects',
        name: 'dashboardAdminProjects',
        component: DashboardAdminProjects,
        beforeEnter: (to, from, next) => {
            if (store.state.user.user.role_app === 'admin') {
                next()
            } else {
                next({ name: 'dashboardUser' })
            }
        }
    },
    {
        path: '/dashboard-admin/user/:id',
        name: 'DashboardAdminUserProjects',
        component: DashboardAdminUser,
        beforeEnter: (to, from, next) => {
            if (store.state.user.user.role_app === 'admin') {
                next()
            } else {
                next({ name: 'dashboardUser' })
            }
        }
    },
    {
        path: '/dashboard-admin/project/:id',
        name: 'DashboardAdminProject',
        component: DashboardAdminProject,
        beforeEnter: (to, from, next) => {
            if (store.state.user.user.role_app === 'admin') {
                next()
            } else {
                next({ name: 'dashboardUser' })
            }
        }
    },
    {
        path: '/projects/:id',
        name: 'project',
        component: Project
    },
    {
        path: '/account',
        name: 'account',
        component: Account
    },
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router
