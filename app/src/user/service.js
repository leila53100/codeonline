import fetch from '../config/fetch'
import axios from "axios";

export const checkTokenFetch = async (token) => {

    const body = {
        token: token
    }

    return await fetch(process.env.VUE_APP_API_URL + '/confirm_token', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(res => {
        if (res != null) {
            return res
        }
    }).catch(err => {
        if (err.json) {
            return err.json.then(json => {
                console.log(json)
            })
        }
    })
}

export const getProjects = async (id) => {
    return await axios.get(process.env.VUE_APP_API_URL + '/users/projects/' + id).then((res) => {
        return res.data
    }).catch(err => {
        if (err.json) {
            return err.json.then(json => {
                console.log(json)
            })
        }
    })
}

export const getMessages = async (id) => {
    return await axios.get(process.env.VUE_APP_API_URL + '/projects/' + id + '/message').then((res) => {
        return res.data
    }).catch(err => {
        if (err.json) {
            return err.json.then(json => {
                console.log(json)
            })
        }
    })
}
