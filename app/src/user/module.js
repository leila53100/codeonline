import {checkTokenFetch, getProjects, getMessages} from './service'

const user = {
    id: null,
    username: '',
    email: '',
    roles: null,
    role_app: null,
    account_type: '',
    token: null,
    isConnected: 'pending',
    projects: [],
    notfication: {},
    chat: {},
}

const mutations = {
    CHECK_TOKEN: (state, res) => {
        if (res.isValid) {
            const user = res.user;
            state.user.id = user.id;
            state.user.email = user.email;
            state.user.username = user.username;
            state.user.role_app = user.role_app;
            state.user.token = user.token;
            state.user.isConnected = true;
            state.user.account_type = user.account_type;
            localStorage.setItem('user-token', user.token)
        } else {
            localStorage.removeItem('user-token')
            state.user.isConnected = false;
        }
    },
    USER_PROJECTS: (state, res) => {
        state.user.projects = res.role;
    },
    PUSH_MESSAGES: (state, message) => {
        if(message.idProject in state.user.chat) {
            state.user.chat[message.idProject].push(message.value) 
        } else {
            Object.assign(state.user.chat, {[message.idProject]: []})
            state.user.chat[message.idProject].push(message.value)
        }
    },
    FILL_MESSAGES: (state, messages) => {
        state.user.chat = {}
        messages.forEach(message => {
            if(message.idProject in state.user.chat) {
                state.user.chat[message.idProject].push(message.value) 
            } else {
                Object.assign(state.user.chat, {[message.idProject]: []})
                state.user.chat[message.idProject].push(message.value)
            }
        });
    }
}

const actions = {
    checkToken: async (store, token) => {
        const res = await checkTokenFetch(token)
        store.commit('CHECK_TOKEN', res)
    },
    noToken: (store) => {
        store.commit('CHECK_TOKEN', {isValid: false})
    },
    getUserProjects: async (store, id) => {
        const data = await getProjects(id);
        store.commit('USER_PROJECTS', data)
    },
    pushMessage: (store, message) => {
        store.commit('PUSH_MESSAGES', message)
    },
    getMessages: async (store, id) => {
        const messages = await getMessages(id)
        store.commit('FILL_MESSAGES', messages)
    }
}

const getters = {
    user: state => state.user,
    userProjects: state => state.user.projects,
    chat: state => state.user.chat,
    filtered: state => filter => state.user.projects.filter(item => item.role === filter)

}

export default {
    state: () => ({user}),
    mutations: mutations,
    actions: actions,
    getters: getters,
}