'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Notification extends Model {

        static associate(models) {
            Notification.belongsTo(models.User, {
                as: "user",
                foreignKey: "user_to_notify",
                unique: true,
            })
        }
    };
    Notification.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        content: {
            type: DataTypes.STRING,
        },
        fromUsername: {
            type: DataTypes.STRING
        }
    }, {
        sequelize,
        modelName: 'Notification',
    });
    return Notification;
};