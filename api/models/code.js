'use strict';
const {
    Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Code extends Model {

        static associate(models) {
            Code.belongsTo(models.Project)
        }
    };
    Code.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        html: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        css: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        js: {
            type: DataTypes.TEXT,
            allowNull: true,
        }
    }, {
        sequelize,
        modelName: 'Code',
    });
    return Code;
};