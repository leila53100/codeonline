'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Role extends Model {

        static associate(models) {
            Role.belongsTo(models.User, {
                as: "user",
                foreignKey: "user_id",
                unique: true,
                onDelete: 'CASCADE',
            }),
            Role.belongsTo(models.Project, {
                as: "project",
                foreignKey: "project_id",
                unique: true,
                onDelete: 'CASCADE',
            })
        }
    };
    Role.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        role: {
            allowNull: false,
            type: DataTypes.ENUM({
                values: ['developer', 'visitor', 'admin']
            }),
            defaultValue: 'visitor'
        },
        slug: {
            allowNull: false,
            type: DataTypes.STRING
        }

    }, {
        sequelize,
        modelName: 'Role',
    });
    return Role;
};