require('dotenv').config()
const express = require('express');
const RouterManager = require("./routes");
const cors = require('cors');
const app = express();
const port = 4000;
const bodyparser = require('body-parser');
const db = require("./models");
const initListeners = require("./listener/index");

const corsOpts = {
    origin: '*',
    
    methods: [
        'GET',
        'POST',
        'PUT',
        'DELETE'
    ],
    
    allowedHeaders: [
        'Content-Type',
    ],
};

app.use(cors(corsOpts))
app.use(bodyparser.json());

RouterManager(app);

db.sequelize.sync().then(() => {
    console.log(" re-sync db.");
});
// // ajout de socket.io
const server = require('http').Server(app)

const io = require('socket.io')(server, {
    cors: {
        origin: process.env.APP_URL,
        methods: ["GET", "POST", "PUT", "DELETE"]
    }
});

initListeners(io)

server.listen(port, () => {
    console.log(`Listening at http://api.localhost:${port}`)
})