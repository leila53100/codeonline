'use strict'
const express = require("express");
const models = require('../models/index');
const User = models.User
const Project = models.Project
const Role = models.Role
const router = express.Router();

router.get("/", (req, res) => {
    User.findAll({})
        .then((data) => res.json(data))
        .catch((res) => res.sendStatus(500));
});

router.get("/:id", (req, res) => {
    User.findByPk(req.params.id)
        .then((data) => (data ? res.json(data) : res.sendStatus(404)))
        .catch((res) => res.sendStatus(500));
});

//get user + user's project
router.get("/projects/:id", (req, res) => {
    User.findByPk(req.params.id, {
        include: [
            {
                model: Role,
                as: 'role',
                attributes: ["role"],
                include: [
                    {
                        model: Project,
                        as: 'project',
                        attributes: ["id","name", "lang", "capacity", "createdBy", "slug"],
                    },
                ]
            }
        ]
    })
        .then((data) => res.json(data))
        .catch((res) => res.sendStatus(500));
});


router.put("/:id", (req, res) => {
    User.update(req.body, {where: {id: req.params.id}, returning: true})
        .then(() => res.sendStatus(200))
        .catch((res) => res.sendStatus(500));
});

router.delete("/:id", (req, res) => {
    User.destroy({
        where: {
            id: req.params.id,
        },
    })
        .then((nbDelete) => (nbDelete ? res.sendStatus(204) : res.sendStatus(404)))
        .catch((res) => res.sendStatus(500));
});

module.exports = router;
