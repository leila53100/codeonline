const {Router} = require("express");
const router = Router();
const models = require('../models/index');
const User = models.User
const jwt = require('jsonwebtoken');
const bcrypt = require("bcryptjs");

router.post("/login_check", (req, res) => {
        const {email, password} = req.body;
        User.findOne({
            where: {'email': email},
        })
        .then((user) => {
            if (!user) {
                return res.status(200).json({
                    error: "email"
                });
            } else {
                bcrypt.compare(req.body.password, user.password).then((valid) => {
                    if (!valid) {
                        return res.status(200).json({
                            error: "password"
                        });
                    } else {
                        const token = jwt.sign(
                            {email: user.email},
                                process.env.JWT_SECRET,
                                {expiresIn: '24h'});
                            res.status(200).json({
                                email: user.email,
                                token: token
                            });
                        }
                    }
                ).catch((err) => err.errors);
            }
        })
        .catch(() => {
            return res.status(200).json({
                error: "unknow"
            });
        });
    }
);

// POST - Create User 
router.post("/users", (req, res) => {
    User.create(req.body)
        .then((data) => res.status(201).json(data))
        .catch((err) => {
            if(err.name == "SequelizeUniqueConstraintError") {
                return res.status(200).json({
                    error: "email"
                });
            } else {
                return err.errors
            }
        });
});

router.post('/confirm_token', (req, res) => {
    const {token} = req.body;
    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET)
        const email = decoded.email;
        User.findOne({
            where: {'email': email}
        }).then(user => {
            if (!user) {
                res.status(200).json({
                    isValid: false,
                })
            } else {
                res.status(200).json({
                    isValid: true,
                    user: {
                        email: user.email,
                        id: user.id,
                        username: user.username,
                        account_type: user.account_type,
                        role_app: user.role_app,
                        token: token,
                    }
                })
            }
        }).catch((err) => err.errors);
    } catch (err) {
        res.status(200).json({
            isValid: false,
        })
    }
})

router.post('/user_from_token', (req, res) => {
    const {token} = req.body;
    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET)
        const email = decoded.email;
        User.findOne({
            where: {'email': email}
        }).then(user => {
            if (!user) {
                res.status(200).json({
                    isValid: false,
                })
            } else {
                res.status(200).json({
                    isValid: true,
                    user: {
                        email: user.email,
                        id: user.id,
                        username: user.username,
                        account_type: user.account_type,
                        token: token,
                    }
                })
            }
        }).catch((err) => err.errors);
    } catch (err) {
        res.status(200).json({
            isValid: false,
        })
    }
})

module.exports = router;

