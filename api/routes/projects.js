const express = require("express");
const models = require('../models/index');
const message = require("../models/message");
const User = models.User
const Project = models.Project
const Role = models.Role
const Code = models.Code
const Message = models.Message
const router = express.Router();
const { uuid } = require('uuidv4');


router.get("/", (req, res) => {
    Project.findAll({})
        .then((data) => res.json(data))
        .catch((res) => res.sendStatus(500));
});
// get project and it's users
router.get("/users/:slug", (req, res) => {
    Project.findOne({
        where: {
            slug: req.params.slug
        },
        include: [
            {
                model: User,
                as: "users",
                attributes: ["id", "username", "email"],
                include: {
                    model: Role,
                    as: 'role',
                    where: {slug: req.params.slug}
                }
            }
        ]
    })
    .then((data) => res.json(data))
    .catch((err) => res.sendStatus(500));
});
// get projects created by user id
router.get('/created_by/:id', (req, res) => {
    Project.findAll({
            where: {createdBy: req.params.id}
        }
    ).then((data) => res.json(data))
        .catch((err) => err.errors);
})

router.post("/:id", (req, res) => {
    Object.assign(req.body, {slug: uuid()})
    Project.create(req.body)
        .then((project) => {
            return User.findByPk(req.params.id).then((user) => {
                if (!user) {
                    console.log("User not found!");
                    return null;
                }
                project.addUser(user).then((data) => {
                    if (data) {
                        Role.create({user_id: user.id, project_id: project.id, role: "admin", slug : req.body.slug})
                            .then((role) => {
                                res.json(role)
                                return role;
                            })
                            .catch((error) => {
                                    error.error
                                }
                            );
                    }
                }).catch(error => {
                    error.error
                })
            })

        }).catch((err) => err.errors);
})


// add user to project
router.post('/add/:projectId', (req, res) => {
    return Project.findOne({
        where: {
            slug: req.params.projectId
        }
    })
        .then((project) => {
            if (!project) {
                console.log("project not found!");
                return null;
            }
            return User.findOne({where: {email: req.body.email}}).then((User) => {
                if (!User) {
                    console.log("User not found!");
                    return null;
                }
                project.addUser(User).then((data) => {
                    Role.create({user_id: User.id, project_id: project.id, role: req.body.role, slug: req.params.projectId})
                    res.json(data)
                    return data;
                }).catch((err) => err.errors);
            });
        }).catch(error => {
            console.log(error)
        })
})

router.post('/remove/:projectId/:userId', (req, res) => {
    return Project.findOne({
        where: {
            slug: req.params.projectId
        }
    })
        .then((project) => {
            if (!project) {
                console.log("project not found!");
                return null;
            }
            return User.findByPk(req.params.userId).then((User) => {
                if (!User) {
                    console.log("User not found!");
                    return null;
                }
                project.removeUser(User).then((data) => {
                    Role.destroy({where: {user_id: User.id, project_id: project.id}})
                    res.json(data)
                    return data;
                }).catch((err) => err.errors);
            });
        }).catch(error => {
            console.log(error)
        })
})


router.put("/:id", (req, res) => {
    Project.update(req.body, {where: {id: req.params.id}})
        .then(() => res.sendStatus(200))
        .catch((res) => res.sendStatus(500));
});

router.delete("/:id", (req, res) => {
    Project.destroy({
        where: {
            id: req.params.id,
        },
    }).then((nbDelete) => {
        if (nbDelete) {
            Role.destroy({
                where: {
                    'project_id': req.params.id,
                },
            })
                .then(() => res.sendStatus(204))
                .catch((res) => res.sendStatus(500))
        } else {
            res.sendStatus(404)
        }
    })
        .catch((res) => res.sendStatus(500));
});

router.get('/:id/code', (req, res) => {
    Code.findOne({where: {ProjectId: req.params.id}
    }).then(code => {
        res.json(code)
    }).catch((res) => res.sendStatus(500))
})

router.get('/:id/message', (req, res) => {
    Message.findAll({where: {ProjectId: req.params.id}
    }).then(messages => {
        data = messages.map(message => {
            return {
                idProject: message.ProjectId,
                value: {
                    from: message.from,
                    content: message.content
                }
            }
        })
        res.json(data)
    }).catch((res) => res.sendStatus(500))
})

module.exports = router;
