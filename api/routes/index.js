const SecurityRouter = require("./security");
const UserRouter = require("./users");
const AdminRouter = require("./users");
const ProjectRouter = require("./projects")
const RoleRouter = require("./role")
const NotificationRouter = require("./notification")
const StripeRouter = require("./stripe")

const routerManager = (app) => {
    app.use("/", SecurityRouter);
    app.use("/users", UserRouter);
    app.use("/projects", ProjectRouter)
    app.use("/roles", RoleRouter)
    app.use("/admin", AdminRouter)
    app.use("/notifications", NotificationRouter)
    app.use("/stripe", StripeRouter)
};

module.exports = routerManager;
