const express = require("express");
const models = require('../models/index');
const Notification = models.Notification
const User = models.User

const router = express.Router();

router.post("/", (req, res) => {
    User.findOne({ where : {email :req.body.user_to_notify}}).then(
        (user) => { if (!user) {
        console.log("User not found!");
        return null;
        } else {
            Notification.create({
                user_to_notify : user.id,
                content : req.body.content,
                fromUsername: req.body.fromUsername
            })
                .then((data) => res.status(201).json(data))
                .catch((err) => err.errors);
        }})

});

router.get("/:email", (req, res) => {
    User.findOne({ where : {email : req.params.email}})
        .then((user) => {
            if (user) {
                return  Notification.findAll(
                    {where : {user_to_notify : user.id }, order: [["createdAt", "DESC"]]},
                    )
                     .then((data) => res.json(data))
                     .catch((res) => res.sendStatus(500));
            }
            }
        )
        .catch((err) => res.sendStatus(500));
});

module.exports = router;
