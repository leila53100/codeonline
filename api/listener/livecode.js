const models = require('../models/index');
const Code = models.Code;

module.exports = function(client, io) {
    client.on('code-send-html', (data) => {
        Code.findOne({where: {ProjectId: data.id}
        }).then(code => {
            if(code) {
                Code.update({html: data.html}, {where: {id: code.id}})
            } else {
                Code.create({ProjectId: data.id, html: data.html})
            }
        });
        io.to(data.slug).emit('code-send-html-back', data.html)
    });

    client.on('code-send-css', (data) => {
        Code.findOne({where: {ProjectId: data.id}
        }).then(code => {
            if(code) {
                Code.update({css: data.css}, {where: {id: code.id}})
            } else {
                Code.create({ProjectId: data.id, css: data.css})
            }
        });
        io.to(data.slug).emit('code-send-css-back', data.css)
    });

    client.on('code-send-js', (data) => {
        Code.findOne({where: {ProjectId: data.id}
        }).then(code => {
            if(code) {
                Code.update({js: data.js}, {where: {id: code.id}})
            } else {
                Code.create({ProjectId: data.id, js: data.js})
            }
        });
        io.to(data.slug).emit('code-send-js-back', data.js)
    });
};