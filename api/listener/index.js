const models = require('../models/index');
const User = models.User

module.exports = function(io) {

    io.on('connection', (client) => {

        console.log(client.id)

        client.on('setup', data => {
            User.findByPk(data.user.id, {
                include: {
                    all: true, nested: true
                }
            })
            .then(user => {
                client.join(user.id)
                client.join(user.email)
                user.projects.forEach(project => {
                    client.join(project.slug)
                });
            })
        })

        client.on('update-tunnel-to-listen', (data) => {
            User.findByPk(data.id, {
                include: {
                    all: true, nested: true
                }
            })
            .then(user => {
                client.join(user.id)
                client.join(user.email)
                user.projects.forEach(project => {
                    client.join(project.slug)
                });
            })
        })

        require('./livechat')(client, io);
        require('./notification')(client, io);
        require('./livecode')(client, io);

        return io;
    })

};